---
layout: 'default'
title: 'Project Structure'
---

## Project Structure

### July 26, 2013

I am using meta-data to include files on a per page basis. I am also using the [node-browserify](https://github.com/substack/node-browserify) package to bundle up my scripts and give myself a common layer that can be leveraged across all pages at once.

This means projects are no longer a single file. 

I am using directories to keep track of each day, and using the same filenames for projects.

	/20130721
	project.html.md
	project.css.styl
	project.js.b

Here's an example `project.html.md`, including the metadata that specifies the projects CSS and JavaScript looks like this.

	---
	layout: 'default'
	title: 'Project'
	css: 'project.css'
	javascript: 'project.js'
	---

	## Project

	<div class="markup"></div>

DocPad uses YAML to define metadata, in a section defined by 3 repeats of a character. I use markdown for the title, and regular markup to create my project.

This is all fed throught the templating system of DocPad. I am using .eco templates for all of that, along with docpad-plugin-partials for the website templating. You can find these templates in the src/layouts and src/partials directories.

### July 25, 2013

Right now each project is only one file. I am using Google CDN for jQuery and putting script and style tags all on one page. I have been copying the files to start each new project.