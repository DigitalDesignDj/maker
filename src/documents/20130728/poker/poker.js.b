require('../../scripts/common.js');

var $         = require('jquery-browserify'),
	ko        = require('knockout'),
	viewModel = require('./viewModel.js');

require('./scoring-bindings.js');
require('./frontend-bindings.js');
require('./localStorage-bindings.js');

ko.applyBindings( viewModel );
