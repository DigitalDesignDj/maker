---
layout: 'default'
title: 'Project'
css: 'poker.css'
javascript: 'simple-poker.js'
---

## Simple Poker

1. You have been dealt a hand of cards, click the ones you want to keep, then press the deal button.

2. There's no scoring, you can refresh the page to shuffle the deck and get a new hand.

3. If you keep pressing the "Deal" button, you will eventually run out of cards. This game uses 1 standard deck of 52 cards. If you want to peek at the deck (cheater!) you can look at `poker.deck` in the console.

<div class="cards"></div>

<button>Deal</button>
