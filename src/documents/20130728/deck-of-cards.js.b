require('../scripts/common.js');
var $ = require('jquery-browserify');

setupCards = {
	number_of_cards: 52,
	suits: ['clubs', 'diamonds', 'hearts', 'spades'],
	values: ['ace', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten', 'jack', 'queen', 'king'],
	deck: [],
	init: function(){
		self = this;
		self.setupCards();
	},
	setupCards: function(){
		self = this;
		for ( var suit_index=0; suit_index<self.suits.length; suit_index++ ){
			for ( var value_index=0; value_index<self.values.length; value_index++ ){
				$( '.cards' ).append(
					$( '<div/>' ).addClass( 
						'card ' 
						+ self.suits[suit_index] 
						+ ' ' 
						+ self.values[value_index]
					)
				);
			}
		}
	}
}

setupCards.init();
