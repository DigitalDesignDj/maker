require('../scripts/common.js');
var $ = require('jquery-browserify');

setupCards = {
	number_of_cards: 52,
	suits: ['clubs', 'diamonds', 'hearts', 'spades'],
	values: ['ace', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten', 'jack', 'queen', 'king'],
	deck: [],
	init: function(){
		self = this;
		self.setupCards();
		self.deck = self.shuffle( self.deck );
		$.each( self.deck, function( i, v ){
			$( '.cards' ).append(
				$( '<div/>' ).addClass( 'card ' + v.suit + ' ' + v.value )
			);
		});
	},
	setupCards: function(){
		self = this;
		for ( var suit_index=0; suit_index<self.suits.length; suit_index++ ){
			for ( var value_index=0; value_index<self.values.length; value_index++ ){
				self.deck.push( {'suit':self.suits[suit_index],'value':self.values[value_index]} )
			}
		}
	},
	shuffle: function(array) {
		var counter = array.length, temp, index;
		// While there are elements in the array
		while (counter > 0) {
			// Pick a random index
			index = (Math.random() * counter--) | 0;
			// And swap the last element with it
			temp = array[counter];
			array[counter] = array[index];
			array[index] = temp;
		}
		return array;
	}
}

setupCards.init();
