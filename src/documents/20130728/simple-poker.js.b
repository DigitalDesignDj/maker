require('../scripts/common.js');
var $ = require('jquery-browserify');

Poker = {
	number_of_cards: 52,
	suits: ['clubs', 'diamonds', 'hearts', 'spades'],
	values: ['ace', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten', 'jack', 'queen', 'king'],
	deck: [],
	hand_size: 5,
	init: function(){
		self = this;
		self.setupCards();
		self.deck = self.shuffle( self.deck );
		self.dealCards( self.hand_size );
		$( '.card' ).on( 'click', function( e ){
			$( e.target ).toggleClass( 'hold' );
		});
		$( 'button' ).on( 'click', function( e ){
			// Remove non-held cards
			var number_removed = 0;
			$( '.card' ).each(function( i, v ){
				if( !$(v).hasClass('hold') ){
					$(v).remove();
					number_removed++;
				}
			});
			console.log( number_removed );
			self.dealCards( number_removed );
		});
	},
	setupCards: function(){
		self = this;
		for ( var suit_index = 0; suit_index < self.suits.length; suit_index++ ){
			for ( var value_index = 0; value_index < self.values.length; value_index++ ){
				self.deck.push( {'suit':self.suits[suit_index],'value':self.values[value_index]} )
			}
		}
	},
	dealCards: function( number ) {
		self = this;
		for ( var hand_index = 0; hand_index < number; hand_index++ ){
			console.log( self.deck[hand_index] );
			$( '.cards' ).append(
				$( '<div/>' ).addClass( 
					'card '
					+ self.deck[hand_index].suit
					+ ' '
					+ self.deck[hand_index].value
				)
			);
			// Remove card from deck
			self.deck.splice(hand_index, 1);
		}
	},
	shuffle: function(array) {
		var counter = array.length, temp, index;
		// While there are elements in the array
		while (counter > 0) {
			// Pick a random index
			index = (Math.random() * counter--) | 0;
			// And swap the last element with it
			temp = array[counter];
			array[counter] = array[index];
			array[index] = temp;
		}
		return array;
	}
}

Poker.init();
