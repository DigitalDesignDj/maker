---
layout: 'default'
title: 'Dynamic Mosaic'
css: 'dynamic-mosaic.css'
javascript: 'dynamic-mosaic.js'
---

## Dynamic Mosaic

1. Hover over the squares to change the colors

<div class="canvas-area"></div>
