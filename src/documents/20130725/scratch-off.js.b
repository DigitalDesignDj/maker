require('../scripts/common.js');
var $ = require('jquery-browserify');

color_filler = {
	number_of_cells: 300,
	init: function(){
		self = this;
		for (var i=0;i<self.number_of_cells;i++){
			$('.canvas-area').append( $('<div/>').addClass("cell") );
		}
		$('.cell').on('mouseover', function( e ){
			self.remove_background_color( $(e.target) );
		});
	},
	remove_background_color: function( elem ) {
		elem.css({'background-color': 'transparent', 'pointer-events':'none' });
	}
}

$(function(){
	color_filler.init();
});