---
layout: 'default'
title: 'Color Filler'
css: 'color-filler.css'
javascript: 'color-filler.js'
---

## Color Filler

1. Select a color
2. Color some squares
3. Refresh the page if you want a clean canvas and new pallete

<div class="pallette">
	<div class="color"></div>
	<div class="color"></div>
	<div class="color"></div>
	<div class="color"></div>
	<div class="color"></div>
	<div class="color"></div>
</div>
<div class="canvas-area">
	<div class="cell"></div>
	<div class="cell"></div>
	<div class="cell"></div>
	<div class="cell"></div>
	<div class="cell"></div>
	<div class="cell"></div>
	<div class="cell"></div>
	<div class="cell"></div>
	<div class="cell"></div>
</div>
