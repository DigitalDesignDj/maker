---
layout: 'default'
title: 'Scratch Off'
css: 'scratch-off.css'
javascript: 'scratch-off.js'
---

## Scratch Off

1. Hover over the squares to scratch off the coating and reveal something

2. If you manage to complete this on an iPad, send me a screenshot and I'll dismiss it as a photoshop job. ~_^

<div class="canvas-area"></div>
