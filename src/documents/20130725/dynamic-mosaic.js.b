require('../scripts/common.js');
var $ = require('jquery-browserify');

color_filler = {
	number_of_cells: 300,
	init: function(){
		self = this;
		for (var i=0;i<self.number_of_cells;i++){
			$('.canvas-area').append( $('<div/>').addClass("cell") );
		}
		$('.cell').on('mouseover', function( e ){
			self.set_random_background_color( $(e.target) );
		});
	},
	get_random_color: function() {
		var letters = '0123456789ABCDEF'.split('');
		var color = '#';
		for (var i = 0; i < 6; i++ ) {
		    color += letters[Math.round(Math.random() * 15)];
		}
		return color;
	},
	set_random_background_color: function( elem ) {
		elem.css({'background-color': this.get_random_color() });
	}
}

color_filler.init();