require('../scripts/common.js');
var $ = require('jquery-browserify');

color_filler = {
	current_color: 'white',
	init: function(){
		self = this;
		// Setup pallete with random colors
		$('.color').each( function( i, elem ) {
			self.set_random_background_color( $(elem) );
		});
		// Pickup color from pallete and save to current_color
		$('.color').on('click', function( e ){
			$('.color').removeClass('active');
			$(e.target).addClass('active');
			self.current_color = $(e.target).css('background-color');
		});
		//
		$('.cell').on('click', function( e ){
			$(e.target).css({'background-color': self.current_color });
		});
	},
	get_random_color: function() {
		var letters = '0123456789ABCDEF'.split('');
		var color = '#';
		for (var i = 0; i < 6; i++ ) {
		    color += letters[Math.round(Math.random() * 15)];
		}
		return color;
	},
	set_random_background_color: function( elem ) {
		elem.css({'background-color': this.get_random_color() });
	}
}

color_filler.init();