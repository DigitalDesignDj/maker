require('../scripts/common.js');
var $ = require('jquery-browserify');

bgcolor = {
	init: function(){
		self = this;
		$('button').on('click', function(){
			self.set_random_background_color( $('body') )
		});
	},
	get_random_color: function() {
		var letters = '0123456789ABCDEF'.split('');
		var color = '#';
		for (var i = 0; i < 6; i++ ) {
		    color += letters[Math.round(Math.random() * 15)];
		}
		return color;
	},
	set_random_background_color: function( elem ) {
		elem.css({'background-color': this.get_random_color() });
	}
}

bgcolor.init();
