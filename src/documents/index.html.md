---
layout: 'default'
title: 'Maker Projects'
date: '20130725'
---

## Maker Projects

### July 29, 2013

GitHub Contributions Hacking

I found a [cool bash script](https://github.com/bd808/profile-life) that lets you create commits that make a pattern in your commit history on GitHub. I took some time to write my name in my commit history.

https://github.com/digitaldesigndj

I have also added alot of work to the poker project from yesterday. I enabled it as an offline webapp at [http://poker.taylordyoung.com](http://poker.taylordyoung.com), added credits and a pay table, and generally debugged and tested the project.

### July 28, 2013

I have added unit testing to the docpad-plugin-browserify plugin.

I also added some animation to the dice projects via [move.js](http://visionmedia.github.io/move.js/).

Todays projects all feature cards!

* [Deck of Cards](20130728/deck-of-cards.html)
* [Shuffled Deck of Cards](20130728/shuffled-cards.html)
* [Simple Poker](20130728/simple-poker.html)
* [Knockout Poker](20130728/poker/)

Simple Poker uses jQuery only. Knockout Poker uses KnockOutJS to manage and couple the view and data.

### July 27, 2013

A dice rolling project.

* [Dice Roller](20130727/dice-roller.html)
* [D9 Dice Roller](20130727/d9-roller.html)

### July 26, 2013

I created a renderer plugin for docpad that will automagically browserify   files specified by adding a `.b` extenstion to them. This allows me to use common js `require()` to specify npm modules and JavaScript files to be used client side. It gives me a way to include regular javascript files into each other and manage dependancies. You can find more information on the [projects page](/projects.html)

* [Docpad Browserify Plugin](https://github.com/digitaldesigndj/docpad-plugin-browserify)

This was my first NPM package submission. [https://npmjs.org/~taylor](https://npmjs.org/~taylor)

To support this, I also added some new metadata for the document templates of this site. Before, I just included JavaScript CSS and HTML all in the same document. Now you can optionally set a relative or absolute path to a JS or CSS file and it will automatially be included.

### July 25, 2013

<!-- The goal here is to create something new everyday. I think it's possible to even have days where I complete a few projects. This site will serve to manage and index these projects. The idea here is to show the world that I make things. I would like to make 365 projects by my next birthday, July 21st. -->

The first group of projects:

* [Random Background Color](20130725/random-background-color.html)
* [Color Filler](20130725/color-filler.html)
* [Dynamic Mosaic](20130725/dynamic-mosaic.html)
* [Scratch Off](20130725/scratch-off.html)

These have only been tested in Chrome and some of them use CSS or JavaScript that is not supported on older browsers. I also know that some of these may not work so well on a phone or in a touch events environment. In short, your mileage may vary... Enojy!

This website was the first part of the project. I would like to build it up into a way that lets me easily manage lots of little html projects and examples. For now I have the CSS, JavaScript, and HTML all in the same file. If I stick to this and keep writing, I'll also add RSS support and other bloggy features.

I also want to make it easier to create these projects, by creating a system that will allow me to create lots of projects in the same environment while keeping things organized.

You can track the progress of the project structure on the <a href="/projects.html">projects</a> page.
