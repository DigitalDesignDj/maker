## Getting Started

1. [Install DocPad](https://github.com/bevry/docpad)

	`npm install -g docpad`

1. Clone the project and run the server

	``` bash
	git clone git://github.com/digitaldesigndj/maker.git
	cd maker
	npm install
	docpad run
	```

1. [Open http://localhost:9778/](http://localhost:9778/)

1. Start hacking away by modifying the `src` directory


## Deploy Process

There are scripts in the [`bin/`](https://github.com/digitaldesigndj/maker/tree/master/bin) directory for building the static site and saving into a git repo called [maker.out](https://github.com/digitaldesigndj/maker.out). This repo hold the final source code for my site at [http://maker.taylordyoung.com](http://maker.taylordyoung.com).


## Grunt

I am using [grunt](http://gruntjs.com/) to copy files as part of the deploy process. I have also installed csslint.

The default task `grunt` runs csslint
